﻿using System;
using System.Threading;

namespace System.Threading.Tasks
{
	public class Task : IAsyncResult
	{
		protected IAsyncResult AsyncResult;

		protected Exception _Exception;

		internal bool FromContinue;

		public bool IsCompleted
		{ get { return AsyncResult != null && AsyncResult.IsCompleted; } }

		public WaitHandle AsyncWaitHandle
		{ get { return AsyncResult == null ? null : AsyncResult.AsyncWaitHandle; } }

		public object AsyncState
		{ get { return AsyncResult == null ? null : AsyncResult.AsyncState; } }

		public bool CompletedSynchronously
		{ get { return AsyncResult != null && AsyncResult.CompletedSynchronously; } }

		public bool IsFaulted { get { return Exception != null; } }

		public Exception Exception { get { return _Exception; } }

		protected readonly Action Action;

		event Action<Task> OnContinue;

		internal Task()
		{
		}

		public Task(Action action)
		{
			Action = action;
		}

		public void Start()
		{
			if (FromContinue)
				throw new InvalidOperationException("Can't call the Start for continue Task.");
			if (AsyncResult != null)
				throw new InvalidOperationException("Task already started.");
			
			_Start();
		}

		internal virtual void _Start()
		{
			Action work = () =>
			{
				try
				{
					if (Action != null)
						Action();
				}
				catch (Exception ex)
				{
					_Exception = ex;
				}
			};
			AsyncResult = work.BeginInvoke(result =>
			{
				work.EndInvoke(result);

				if (OnContinue != null)
					OnContinue(this);
			}, null);
		}

		public Task ContinueWith(Action<Task> action)
		{
			var nextTask = new Task(() => action(this)) { FromContinue = true } ;
			OnContinue += task => nextTask._Start();
			return nextTask;
		}

		public Task<TResult> ContinueWith<TResult>(Func<Task, TResult> func)
		{
			var nextTask = new Task<TResult>(() => func(this)) { FromContinue = true } ;
			OnContinue += task => nextTask._Start();
			return nextTask;
		}

		public static Task Run(Action action)
		{
			var task = new Task(action);
			task.Start();
			return task;
		}

		public static Task<TResult> Run<TResult>(Func<TResult> func)
		{
			var task = new Task<TResult>(func);
			task.Start();
			return task;
		}
	}

	public class Task<TResult> : Task
	{
		protected readonly Func<TResult> Func;

		protected TResult _Result;

		public TResult Result
		{
			get
			{
				while (!IsCompleted)
					Thread.Sleep(1);
				if (_Exception != null)
					throw _Exception;
				return _Result;
			}
		}

		new event Action<Task<TResult>> OnContinue;

		public Task(Func<TResult> func)
		{
			Func = func;
		}

		internal override void _Start()
		{
			Action work = () =>
			{
				try
				{
					if (Func != null)
						_Result = Func();
				}
				catch (Exception ex)
				{
					_Exception = ex;
					_Result = default(TResult);
				}
			};
			AsyncResult = work.BeginInvoke(result =>
			{
				work.EndInvoke(result);

				if (OnContinue != null)
					OnContinue(this);
			}, null);
		}

		public Task ContinueWith(Action<Task<TResult>> action)
		{
			var nextTask = new Task(() => action(this)) { FromContinue = true };
			OnContinue += task => nextTask._Start();
			return nextTask;
		}

		public Task<TNewResult> ContinueWith<TNewResult>(Func<Task<TResult>, TNewResult> func)
		{
			var nextTask = new Task<TNewResult>(() => func(this)) { FromContinue = true } ;
			OnContinue += task => nextTask._Start();
			return nextTask;
		}
	}
}
