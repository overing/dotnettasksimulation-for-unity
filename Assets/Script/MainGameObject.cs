﻿using System;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class MainGameObject : MonoBehaviour
{
	IEnumerator Start()
	{
		Debug.Log("Start task.");
		var longTimeWork = Task.Run(() =>
		{
			int value = new System.Random().Next(100);
			Thread.Sleep(TimeSpan.FromSeconds(3));

			if (value > 80)
				throw new Exception("throw expection test.");

			return value;
		});

		Debug.Log("Other work.");

		Debug.Log("Wait task completed.");
		yield return StartCoroutine(longTimeWork.GetProcessEnumerator());

		if (longTimeWork.IsFaulted)
			Debug.LogError(longTimeWork.Exception);
		else
			Debug.LogWarning(longTimeWork.Result);
	}
}

static class AsyncResultExtension
{
	public static IEnumerator GetProcessEnumerator(this IAsyncResult async)
	{
		while (!async.IsCompleted)
			yield return null;
	}
}
